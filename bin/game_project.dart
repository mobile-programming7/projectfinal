import 'dart:io';
import 'dart:math';
import 'package:game_project/game_project.dart' as game_project;

void main() {
  print("input Name Character");
  String? name = stdin.readLineSync()!;
  MainChacter mainChacter = new MainChacter(name);
  MainBoss mainBoss = new MainBoss();
  print("StatHero");
  print("พลังโจมตี: " + mainChacter.getAtk.toString());
  print("พลังชีวิต: " + mainChacter.getHp.toString());
  print("เลเวล: " + mainChacter.getLevel.toString());
  print("ค่าประสบการณ์: " + mainChacter.getExp.toString());
  print("");
  print("input stage");
  int? stage = int.parse(stdin.readLineSync()!);
  tower(stage);

}

void tower(int stage) {
  print("input Name Character");
  String? name = stdin.readLineSync()!;
  MainChacter mainChacter = new MainChacter(name);
  MainBoss mainBoss = new MainBoss();
  MainMonster mainMonster = new MainMonster();

  int atkMain = mainChacter.getAtk;
  int hpMain = mainChacter.getHp;

  int atkBoss = mainBoss.getAtk + stage;
  int hpBoss = mainBoss.getHp + (stage * 5);
  int levelBoss = mainBoss.getLevel + stage;
  int expDrop = 50 * stage;
  print("Boss and Monster stage : " + stage.toString());
  print("พลังโจมตี: " + atkBoss.toString());
  print("พลังชีวิต: " + hpBoss.toString());
  print("เลเวล: " + levelBoss.toString());

  int atkMon = mainMonster.getAtk + stage;
  int hpMon = mainMonster.getHp + (stage * 5);
  int levelMon = mainMonster.getLevel + stage;
  int MonsterexpDrop = 25 * stage;
  print("พลังโจมตี: " + atkMon.toString());
  print("พลังชีวิต: " + hpMon.toString());
  print("เลเวล: " + levelMon.toString());

  print("input 1 attack boss and monster");
  int type = int.parse(stdin.readLineSync()!);
  int count = 0;
  while (mainMonster.getHp != 0) {
    if (type == 1) {
      print("ฮีโร่โจมตี: " + atkMain.toString());
      hpMon = hpMon - atkMain;
      print("พลังชีวิตมอนเตอร์: " + hpMon.toString());
      count++;
      if (count % 2 == 0) {
        print("มอนเตอร์โจมตี: " + atkMon.toString());
        hpMain = hpMain - atkBoss;
        print("พลังชีวิตฮีโร่: " + hpMain.toString());
      }
    } else {
      print("input 1 to Attack");
    }
    if (hpMon <= 0) {
      print("Monster Die !!!");
      int exp = mainChacter.getExp + MonsterexpDrop;
      print("Exp: " + exp.toString());
      break;
    }
     print("input 1 Attack monster");
    type = int.parse(stdin.readLineSync()!);
  }

  while (mainBoss.getHp != 0) {
    if (type == 1) {
      print("ฮีโร่โจมตี: " + atkMain.toString());
      hpBoss = hpBoss - atkMain;
      print("พลังชีวิตบอส: " + hpBoss.toString());
      count++;
      if (count % 2 == 0) {
        print("บอสโจมตี: " + atkBoss.toString());
        hpMain = hpMain - atkBoss;
        print("พลังชีวิตฮีโร่: " + hpMain.toString());
      }
    } else {
      print("input 1 to Attack");
    }
    if (hpBoss <= 0) {
      print("Boss Die !!!");
      int exp = mainChacter.getExp + expDrop;
      print("You Win");
      print("Exp: " + exp.toString());
      break;
    }
    print("input 1 Attack Boss");
    type = int.parse(stdin.readLineSync()!);
  }

  print("input 1 next stage or input 2 Out Tower");
  int? no = int.parse(stdin.readLineSync()!);
    if(no == 1){
      print("input stage");
      int? stage = int.parse(stdin.readLineSync()!);
      tower(stage);
    }else{
      print("End Game!!!");
    }

}

abstract class Stat {
  int? atk;
  int? hp;
  int? exp;
  int? level;

  Stat(int atk, hp, exp, level) {
    this.atk = atk;
    this.hp = hp;
    this.exp = exp;
    this.level = level;
  }

  get getAtk => this.atk;

  set setAtk(atk) => this.atk = atk;

  get getHp => this.hp;

  set setHp(hp) => this.hp = hp;

  get getExp => this.exp;

  set setExp(exp) => this.exp = exp;

  get getLevel => this.level;

  set setLevel(level) => this.level = level;
}

class MainChacter extends Stat {
  String? name;
  MainChacter(name) : super(20, 100, 0, 0);
}

class MainBoss extends Stat {
  MainBoss() : super(20, 100, 100, 1);
}

class MainMonster extends Stat {
  MainMonster() : super(10, 50, 50, 1);
}
